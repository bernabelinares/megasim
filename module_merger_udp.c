/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Simulating event-driven AER systems with MegaSim workgroup, Cognitive
 neuromorphic workshop in Sardinia 2016.
 
 http://capocaccia.iniforum.ch/2016/workgroups/simulating-event-driven-address-event-representation-aer-systems-with-megasim/
 
 */

#include "megasim.h"
//#include<arpa/inet.h> 
#include <netinet/in.h> // definres struct sockaddr_in
#include <sys/socket.h>
 #include <sys/types.h>
#include <arpa/inet.h> // defines inet_addr() that receives a char array with an ip ("127.0.0.1") and converts to an integer

#define UDP_PARAMS 8
#define PARAMS_PER_IN_PORT 4 
#define INT_PARAMS 9

 #define UDP_STATES 1


const unsigned int xmask = 0x00fe;
const unsigned int xshift = 1;
const unsigned int ymask = 0x7f00;
const unsigned int yshift = 8;
const unsigned int pmask = 0x1;
const unsigned int pshift = 0;

int module_merger_udp(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes,node** onodes,
		prmss* params,prmss* state)
{
	// whenever there are events at input ports at time time_act ready to process, these event will be copied to all output ports a number of times
	//
	// 7+2 parameters in *params:
	// n_params int = 7 : number of params int
	// n_params double = 0 : number of params double
	// n_in_ports: number of input ports
	// n_out_ports: number of output ports
	// delay_to_process: time to process an input event: will set time_busy as well as t_pre_rqst of output events
	// delay_to_ack: time to acknowledge an input request
	// fifo_depth: number of input events that can be accummulated and processed: time_busy - time_act <= fifo_depth * delay_to_process. If this is not met, then ack should acknowledged late enough to guarantee fifo has space for a next event
	// n_repeat: number of times each input event is repeated at an output port
	// delay_to_repeat: delay of repeating output events
	//
	// 2+1+n_in_ports state variables in *state:
	// 0: number of int state variables
	// 1: number of float state variables
	// 2: time_busy: time until latest event will finish processing
	// for each input port: state of preliminary ack
	//
	// time_act: present simulation time. This function will not change it (not a pointer)
	// 
	// inodes: pointer to sub-list of input nodes. 
	// onodes: pointer to sub-list of output nodes.
	// 
	// *ev_in_ports: int vector, one component per input port. Each component indicates if there is an input event at this input port at time time_act (1) or not (0)
	// *ev_out_port: int vector, one component per output port. Each component indicates the number of output events written on this output port at time time_act
	//
	
	int n_in_ports,n_out_ports,n_repeat,delay_to_process,delay_to_ack,delay_to_repeat,fifo_depth,time_busy;
	int in_port,out_port,nin, alert;
	int ev[EVENT_COMPS],evo[EVENT_COMPS];
	
	int ip_addr, port, sequence_number_enabled, address_first_enabled, enable_udp_transmission,
		use_4_byte_addrs, include_timestamps, buffer_size;

	int Nx_array, Ny_array,Dx,Dy;

	struct sockaddr_in si_other;
	int s, i, slen=sizeof(si_other);
    int event_jaer[2],POL;

	n_in_ports       = params->prms[0+2];
	if(ev_in_ports[n_in_ports] != -1){
		printf("Error in module_copy: ev_in_ports[%d] != -1\n\n",n_in_ports);
		exit(24);
	}
	n_out_ports      = params->prms[1+2];
	delay_to_process = params->prms[2+2]; //affects new time_busy and t_pre_rqst of output events
	delay_to_ack     = params->prms[3+2];
	fifo_depth       = params->prms[4+2];
	n_repeat         = params->prms[5+2];
	delay_to_repeat  = params->prms[6+2]; //error

	enable_udp_transmission = params->prms[3+5+1];
	port = params->prms[3+5+3];
	ip_addr= params->prms[3+5+2];
	time_busy = state->prms[0+2];
	address_first_enabled = params->prms[3+5+5];//5 
	include_timestamps = params->prms[3+5+7];
	int tmp,tmp_evnt;
	for(in_port=0;in_port<n_in_ports;in_port++){ //process all simultaneous input events on active input ports
		nin = ev_in_ports[in_port]; //'1' if there is input event, '0' otherwise
		if(nin != 0 && nin != 1){printf("Error in module_copy: nin != 0,1\n\n");exit(25);}
		if(nin == 1){
			alert = prelim_handshake(time_act,&(prelim_ack[in_port]),ev,inodes[in_port],&time_busy,delay_to_process,delay_to_ack,fifo_depth);
			if(alert == 0){
				int k = 3+5+9;

				Dx = params->prms[3+5+8+3+(in_port*4)];
				Dy = params->prms[3+5+8+4+(in_port*4)];
				//printf("port: %d, nx:%d, ny:%d, dx:%d, dy:%d, enable:%d\n",port,Nx_array,Ny_array,Dx,Dy,enable_udp_transmission);

				evo[0]=ev[0]+Dx;
				evo[1]=ev[1]+Dy;
				if (ev[2]==-1)
				    POL=0;
				else
				    POL=1;
				
				evo[2]=ev[2];
				//port = params->prms[]
				if (enable_udp_transmission)
				{
					//convert event to jaer format
					// event =Y <<yshift
					// event = event|(X<<xshift)
					// event = event |POL
					// event = (0xffFF<<16)|event
					POL = abs(evo[2] -1 );
					event_jaer[0]=evo[1]<<yshift;
					event_jaer[0]=event_jaer[0]|(evo[0]<<xshift);
					event_jaer[0]=event_jaer[0]|POL;
					event_jaer[0]= (0xffFF<<16)|event_jaer[0];
					tmp_evnt=event_jaer[0];
					event_jaer[1]=time_act;					

					// convert to big endian 
					if (address_first_enabled)
					{
						event_jaer[0] = htonl(event_jaer[0]);
						event_jaer[1]=htonl(event_jaer[1]);
					}
					else
					{
						tmp = htonl(event_jaer[0]);
						event_jaer[0] = htonl(event_jaer[1]);
						event_jaer[1]=tmp;
					}
					tmp_evnt= htonl(tmp_evnt);

					// this struct is needed by the sendto command
	    			struct sockaddr_in myaddr;
	    			memset( (char *)&myaddr, 0, sizeof(myaddr) );
	    			myaddr.sin_family = AF_INET;
	    			myaddr.sin_addr.s_addr = ip_addr;
	    			myaddr.sin_port=htons(port);
	    			//udp socket created in the state params
	    			s = state->prms[n_in_ports+3];
	    			if (include_timestamps)
	    			{
	    				sendto(s, event_jaer, 8, 0,
	    					(struct sockaddr *)&myaddr,sizeof(myaddr) );
	    			}
	    			else
	    			{
	    				sendto(s, tmp_evnt, 4, 0,
	    					(struct sockaddr *)&myaddr,sizeof(myaddr) );	    				
	    			}
    			}//if udp transmission is enabled

 				time_busy = max(time_busy,write_event(time_busy,evo,onodes[0],n_repeat,delay_to_repeat));

				//for(out_port=0;out_port<n_out_ports;out_port++)
				//	time_busy = max(time_busy,write_event(time_busy,ev,onodes[out_port],n_repeat,delay_to_repeat));
				state->prms[2+1+in_port] = -1;
				state->prms[0+2] = time_busy;
			}
			else
				state->prms[2+1+in_port] = 1;
		}
	}
	
	return 0;
}


int module_merger_udp_params(prmss *params, char *prm_file_name){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH];
	int i, total_integer_params=9, num, *tmp, ok[7]={0,0,0,0,0,0,0}, okk=1;

	char ip[16];
	int ip_size=20;
	int port, sequence_number_enabled, address_first_enabled, enable_udp_transmission,
	use_4_byte_addrs,include_timestamps,buffer_size;
	int Nx_array,Ny_array,Dx,Dy,n_in_ports,n_out_ports;

	int p_ip_low32;
	int p_ip_high32;
	long unsigned int *p_ip_longu;
	int cc;
	// parameter of module_copy are 7+2:
	// n_int_params 7, total number of integer params used in module_copy 
	// n_double_params 0, total number of floating point params used in module_copy
	//
	// previous params are fixed and cannot be changed by the user in the parameters file.
	// the following parameters are configurable by the user through the parameter file:
	//
	// n_in_ports, number of input ports
	// n_out_ports, number of ouput ports (each event in an input port is replicated at all output ports
	// delay_to_process, delay between an input event is read (Rqst activated) and its pre-Rqst appears at the output ports
	// delay_to_ack, delay between an input event is read (Rqst activated) and it is acknowledged (Ack activated)
	// fifo_depth, number of input events that can be accummulated and processed
	// n_repeat, number of times each input event is repeated at an output port
	// delay_to_repeat, delay between repeating output events

	cc=0;	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_copy_params\n\n",prm_file_name);
		exit(63);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of parameter file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(64);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in parameter file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(65);
	}
	int new_size=(7+2);
	tmp = (int *) realloc((*params).prms, new_size*sizeof(int));
	(*params).prms = tmp;
	(*params).prms[cc++] = total_integer_params - 2; // n_int_params 7
	(*params).prms[cc++] = 0; // n_double_params 0
	
	//printf("SIZE IS h %f\n",malloc_usable_size(params->prms)/(float)4); //9
	
	for(i=2;i<total_integer_params;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer params in file %s. It must be name_of_param followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(66);
		}
	
		if(strcmp(s,"n_in_ports") == 0){
			check_if_ok(&(ok[2-2]), "n_in_ports", prm_file_name);
			(*params).prms[cc++] = num;
			n_in_ports=num;
		}
		
		if(strcmp(s,"n_out_ports") == 0){
			check_if_ok(&(ok[3-2]), "n_out_ports", prm_file_name);
			(*params).prms[cc++] = num;
		}
			
		if(strcmp(s,"delay_to_process") == 0){
			check_if_ok(&(ok[4-2]), "delay_to_process", prm_file_name);
			(*params).prms[cc++] = num;
		}
			
		if(strcmp(s,"delay_to_ack") == 0){
			check_if_ok(&(ok[5-2]), "delay_to_ack", prm_file_name);
			(*params).prms[cc++] = num;
		}
			
		if(strcmp(s,"fifo_depth") == 0){
			check_if_ok(&(ok[6-2]), "fifo_depth", prm_file_name);
			(*params).prms[cc++] = num;
		}
			
		if(strcmp(s,"n_repeat") == 0){
			check_if_ok(&(ok[7-2]), "n_repeat", prm_file_name);
			(*params).prms[cc++] = num;
		}
			
		if(strcmp(s,"delay_to_repeat") == 0){
			check_if_ok(&(ok[8-2]), "delay_to_repeat", prm_file_name);
			(*params).prms[cc++] = num;
		}
		
	}
	
	for(i=2;i<total_integer_params;i++)
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading parameters from %s: some parameter is missing or file structure is wrong\n\n",prm_file_name);
//		exit(67);
	}

	new_size = new_size +UDP_PARAMS;
        tmp = (int *) realloc((*params).prms, (new_size)*sizeof(int));

        if (tmp==NULL){
        printf("null\n");
        exit(1);
        }
        
	(*params).prms = tmp;
       // printf("SIZE IS %f\n",malloc_usable_size(params->prms)/(float)4);
	if(fscanf(fp,"enable_udp_transmission %d\n",&enable_udp_transmission) != 1){
			printf("Error reading enable_udp_transmission in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = enable_udp_transmission;

	if(fscanf(fp,"hostname %s\n",&ip) != 1){
			printf("Error reading ip in file %s.\n\n",prm_file_name);
			exit(10);}
    		(*params).prms[cc++]=inet_addr(ip);

	if(fscanf(fp,"port %d\n",&port) != 1){
			printf("Error reading port in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = port;
	if(fscanf(fp,"sequence_number_enabled %d\n",&sequence_number_enabled) != 1){
			printf("Error reading sequence_number_enabled in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = sequence_number_enabled;
	if(fscanf(fp,"address_first_enabled %d\n",&address_first_enabled) != 1){
			printf("Error reading address_first_enabled in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = address_first_enabled;
	if(fscanf(fp,"use_4_byte_addrs %d\n",&use_4_byte_addrs) != 1){
			printf("Error reading use_4_byte_addrs in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = use_4_byte_addrs;
	if(fscanf(fp,"include_timestamps %d\n",&include_timestamps) != 1){
			printf("Error reading include_timestamps in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = include_timestamps;
	if(fscanf(fp,"buffer_size %d\n",&buffer_size) != 1){
			printf("Error reading buffer_size in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = buffer_size;


	 new_size = new_size +(5*n_in_ports);
	tmp = (int *) realloc((*params).prms, ( new_size)*sizeof(int));
	
	
	if(tmp==NULL){
	printf("error\n");
	exit(1);
	}
	
	(*params).prms = tmp;

       // printf("SIZE IS %f\n",malloc_usable_size(params->prms)/(float)4);
	for(i=0;i<n_in_ports;i++){
		if(fscanf(fp,"Nx_array %d\n",&Nx_array) != 1){
			printf("Error reading Nx_array in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = Nx_array;
		if(fscanf(fp,"Ny_array %d\n",&Ny_array) != 1){
			printf("Error reading Ny_array in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = Ny_array;
		if(fscanf(fp,"Dx %d\n",&Dx) != 1){
			printf("Error reading Dx in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = Dx;
		if(fscanf(fp,"Dy %d\n",&Dy) != 1){
			printf("Error reading Dy in file %s.\n\n",prm_file_name);
			exit(10);}
		(*params).prms[cc++] = Dy;
		}

	(*params).prms[0] = cc; // n_int_params

	// for (i=0;i<params->prms[0];i++){
	// printf("%d: %d\n",i,params->prms[i]);
	// }

	fclose(fp);
       // printf("SIZE IS %f\n",malloc_usable_size(params->prms)/(float)4);
	//exit(1);
	
	
	return 0;
}

int module_merger_udp_states(prmss *states, char *prm_file_name, prmss *params){ //int n_in_ports){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH];
	int n_in_ports;
	int i, num, *tmp, ok[1]={0}, okk=1;
	int socket_s;
	int cc=0;
	// int states of module_copy are 3 + number of input ports (each in port has a prelim_ack state): total int state variables, total float state variables, time_busy
	// (*states).prms[0] = 2+1+(params[i].prms)[0+2]; //total number of int state variables in module_copy
	// (*states).prms[1] = 0; //total number of double state variables in module_copy
	// (*states).prms[2] = 7;  //time_busy
	// (*states.prms)[3+j] = -1; //flag to indicate event on input port j has been prelim-acked (-1) or is pending (1) because module was busy and fifo full
	
	n_in_ports = (*params).prms[2]; // number of input ports

	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_copy_params\n\n",prm_file_name);
		exit(68);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of states file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(69);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in states file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(70);
	}
	
	tmp = (int *) realloc((*states).prms, (3+n_in_ports)*sizeof(int));
	(*states).prms = tmp;
	(*states).prms[cc++] = 3 + n_in_ports; // total number of int state variables in module_copy
	(*states).prms[cc++] = 0; // total number of double state variables in module_copy
	
	for(i=0;i<1;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer initial state in file %s. It must be name_of_state followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(71);
		}
	
		if(strcmp(s,"time_busy_initial") == 0){
			check_if_ok(&(ok[0]), "time_busy_initial", prm_file_name);
			(*states).prms[cc++] = num;
		}
	}
	
	for(i=0;i<1;i++)
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading initial state from %s: some state is missing or file structure is wrong\n\n",prm_file_name);
		exit(72);
	}
	
	// initialize prelim_Ack state to -1 for all input ports
	for(i=0;i<n_in_ports;i++)
		(*states).prms[cc++] = -1; //i+3
	
	int new_size = cc +UDP_STATES;
	tmp = (int *) realloc((*states).prms, (new_size)*sizeof(int));
	(*states).prms = tmp;


	//create udp socket
	 if ( (socket_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		{
			printf("Socket creation failed\n");
			exit(1);
		}

	states->prms[cc++] = socket_s;


	states->prms[0]=cc;
	fclose(fp);

	// printf("States %d size is %d\n",n_in_ports+3,new_size);
	// for (int i=0;i<states->prms[0];i++){
	// 	printf("%d %d\n",i,states->prms[i]);
	// }
	return 0;
}
