/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco
 
 */

#include "megasim.h"

int module_copy(int time_act,int *prelim_ack,int *ev_in_ports,node** inodes,node** onodes,
		prmss* params,prmss* state)
{
	// whenever there are events at input ports at time time_act ready to process, these event will be copied to all output ports a number of times
	//
	// 7+2 parameters in *params:
	// n_params int = 7 : number of params int
	// n_params double = 0 : number of params double
	// n_in_ports: number of input ports
	// n_out_ports: number of output ports
	// delay_to_process: time to process an input event: will set time_busy as well as t_pre_rqst of output events
	// delay_to_ack: time to acknowledge an input request
	// fifo_depth: number of input events that can be accummulated and processed: time_busy - time_act <= fifo_depth * delay_to_process. If this is not met, then ack should acknowledged late enough to guarantee fifo has space for a next event
	// n_repeat: number of times each input event is repeated at an output port
	// delay_to_repeat: delay of repeating output events
	//
	// 2+1+n_in_ports state variables in *state:
	// 0: number of int state variables
	// 1: number of float state variables
	// 2: time_busy: time until latest event will finish processing
	// for each input port: state of preliminary ack
	//
	// time_act: present simulation time. This function will not change it (not a pointer)
	// 
	// inodes: pointer to sub-list of input nodes. 
	// onodes: pointer to sub-list of output nodes.
	// 
	// *ev_in_ports: int vector, one component per input port. Each component indicates if there is an input event at this input port at time time_act (1) or not (0)
	// *ev_out_port: int vector, one component per output port. Each component indicates the number of output events written on this output port at time time_act
	//
	
	int n_in_ports,n_out_ports,n_repeat,delay_to_process,delay_to_ack,delay_to_repeat,fifo_depth,time_busy;
	int in_port,out_port,nin, alert;
	int ev[EVENT_COMPS];
	
	n_in_ports       = params->prms[0+2];
	if(ev_in_ports[n_in_ports] != -1){
		printf("Error in module_copy: ev_in_ports[%d] != -1\n\n",n_in_ports);
		exit(24);
	}
	n_out_ports      = params->prms[1+2];
	delay_to_process = params->prms[2+2]; //affects new time_busy and t_pre_rqst of output events
	delay_to_ack     = params->prms[3+2];
	fifo_depth       = params->prms[4+2];
	n_repeat         = params->prms[5+2];
	delay_to_repeat  = params->prms[6+2];

	time_busy = state->prms[0+2];
	
	for(in_port=0;in_port<n_in_ports;in_port++){ //process all simultaneous input events on active input ports
		nin = ev_in_ports[in_port]; //'1' if there is input event, '0' otherwise
		if(nin != 0 && nin != 1){printf("Error in module_copy: nin != 0,1\n\n");exit(25);}
		if(nin == 1){
			alert = prelim_handshake(time_act,&(prelim_ack[in_port]),ev,inodes[in_port],&time_busy,delay_to_process,delay_to_ack,fifo_depth);
			if(alert == 0){
				for(out_port=0;out_port<n_out_ports;out_port++)
					time_busy = max(time_busy,write_event(time_busy,ev,onodes[out_port],n_repeat,delay_to_repeat));
				state->prms[2+1+in_port] = -1;
				state->prms[0+2] = time_busy;
			}
			else
				state->prms[2+1+in_port] = 1;
		}
	}
	
	return 0;
}


int module_copy_params(prmss *params, char *prm_file_name){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH];
	int i, total_integer_params=9, num, *tmp, ok[7]={0,0,0,0,0,0,0}, okk=1;
	
	// parameter of module_copy are 7+2:
	// n_int_params 7, total number of integer params used in module_copy 
	// n_double_params 0, total number of floating point params used in module_copy
	//
	// previous params are fixed and cannot be changed by the user in the parameters file.
	// the following parameters are configurable by the user through the parameter file:
	//
	// n_in_ports, number of input ports
	// n_out_ports, number of ouput ports (each event in an input port is replicated at all output ports
	// delay_to_process, delay between an input event is read (Rqst activated) and its pre-Rqst appears at the output ports
	// delay_to_ack, delay between an input event is read (Rqst activated) and it is acknowledged (Ack activated)
	// fifo_depth, number of input events that can be accummulated and processed
	// n_repeat, number of times each input event is repeated at an output port
	// delay_to_repeat, delay between repeating output events

	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_copy_params\n\n",prm_file_name);
		exit(63);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of parameter file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(64);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in parameter file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(65);
	}
	
	tmp = (int *) realloc((*params).prms, (7+2)*sizeof(int));
	(*params).prms = tmp;
	(*params).prms[0] = total_integer_params - 2; // n_int_params 7
	(*params).prms[1] = 0; // n_double_params 0
	
	for(i=2;i<total_integer_params;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer params in file %s. It must be name_of_param followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(66);
		}
	
		if(strcmp(s,"n_in_ports") == 0){
			check_if_ok(&(ok[2-2]), "n_in_ports", prm_file_name);
			(*params).prms[2] = num;
		}
		
		if(strcmp(s,"n_out_ports") == 0){
			check_if_ok(&(ok[3-2]), "n_out_ports", prm_file_name);
			(*params).prms[3] = num;
		}
			
		if(strcmp(s,"delay_to_process") == 0){
			check_if_ok(&(ok[4-2]), "delay_to_process", prm_file_name);
			(*params).prms[4] = num;
		}
			
		if(strcmp(s,"delay_to_ack") == 0){
			check_if_ok(&(ok[5-2]), "delay_to_ack", prm_file_name);
			(*params).prms[5] = num;
		}
			
		if(strcmp(s,"fifo_depth") == 0){
			check_if_ok(&(ok[6-2]), "fifo_depth", prm_file_name);
			(*params).prms[6] = num;
		}
			
		if(strcmp(s,"n_repeat") == 0){
			check_if_ok(&(ok[7-2]), "n_repeat", prm_file_name);
			(*params).prms[7] = num;
		}
			
		if(strcmp(s,"delay_to_repeat") == 0){
			check_if_ok(&(ok[8-2]), "delay_to_repeat", prm_file_name);
			(*params).prms[8] = num;
		}
		
	}
	
	for(i=2;i<total_integer_params;i++)
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading parameters from %s: some parameter is missing or file structure is wrong\n\n",prm_file_name);
		exit(67);
	}
	
	fclose(fp);
	
	return 0;
}

int module_copy_states(prmss *states, char *prm_file_name, prmss *params){ //int n_in_ports){
	
	FILE *fp;
	char s[PARAMETERFILE_MAX_LINE_LENGTH];
	int n_in_ports;
	int i, num, *tmp, ok[1]={0}, okk=1;
	
	// int states of module_copy are 3 + number of input ports (each in port has a prelim_ack state): total int state variables, total float state variables, time_busy
	// (*states).prms[0] = 2+1+(params[i].prms)[0+2]; //total number of int state variables in module_copy
	// (*states).prms[1] = 0; //total number of double state variables in module_copy
	// (*states).prms[2] = 7;  //time_busy
	// (*states.prms)[3+j] = -1; //flag to indicate event on input port j has been prelim-acked (-1) or is pending (1) because module was busy and fifo full
	
	n_in_ports = (*params).prms[2]; // number of input ports
	
	if((fp = fopen(prm_file_name,"r")) == NULL){
		printf("Error opening file %s in module_copy_params\n\n",prm_file_name);
		exit(68);
	}
	if(fscanf(fp,"%s\n",s) != 1){
		printf("Error reading first line of states file %s. First line must be .integers (without spaces)\n\n",prm_file_name);
		exit(69);
	}
	if(strcmp(s,".integers") != 0){
		printf("Error in states file %s: first line must be .integers (without spaces)\n\n",prm_file_name);
		exit(70);
	}
	
	tmp = (int *) realloc((*states).prms, (3+n_in_ports)*sizeof(int));
	(*states).prms = tmp;
	(*states).prms[0] = 3 + n_in_ports; // total number of int state variables in module_copy
	(*states).prms[1] = 0; // total number of double state variables in module_copy
	
	for(i=0;i<1;i++){
		if(fscanf(fp,"%s %d\n",s,&num) != 2){
			printf("Error reading integer initial state in file %s. It must be name_of_state followed by space and a number without spaces afterwards\n\n",prm_file_name);
			exit(71);
		}
	
		if(strcmp(s,"time_busy_initial") == 0){
			check_if_ok(&(ok[0]), "time_busy_initial", prm_file_name);
			(*states).prms[2] = num;
		}
	}
	
	for(i=0;i<1;i++)
		if(ok[i] == 0)
			okk = 0;
	if(okk == 0){
		printf("Error reading initial state from %s: some state is missing or file structure is wrong\n\n",prm_file_name);
		exit(72);
	}
	
	// initialize prelim_Ack state to -1 for all input ports
	for(i=0;i<n_in_ports;i++)
		(*states).prms[i+3] = -1;
	
	fclose(fp);
	
	return 0;
}
