/*
 
 Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
 Author: Bernabe Linares-Barranco

 Function pointer definitions required by the modules
 
*/

#ifndef __MODULE_GENERAL_H__
#define __MODULE_GENERAL_H__

#include "megasim.h"

// Functions (Modules) declarations
//typedef int (*functionPointer)(int ,int *,int *,node** ,node** ,int *,int *,double *,double *);
typedef int (*functionPointer)(int ,int *,int *,node** ,node** ,prmss*,prmss*);
	int module_copy(); //declare here all available modules
	int module_conv();
	int module_conv_plastic();
	int module_stdp();
    int module_stdp_orderbased();
    int module_fully_connected();
    int module_fully_connected_plastic();
    int module_flatten();
    int module_reshape();
    int module_merger_udp();
    int module_split_events();
	int totalFuncs = 11; //indicate total number of available modules
	char *FunctionsCatalog[] = {"module_copy","module_conv","module_conv_plastic",
	"module_stdp","module_stdp_orderbased","module_fully_connected",
	"module_fully_connected_plastic","module_flatten","module_reshape",
	"module_merger_udp","module_split_events"}; //strings with modules names
	functionPointer funPtrCatalog[] = {&module_copy,&module_conv,
	&module_conv_plastic,&module_stdp,&module_stdp_orderbased,
	&module_fully_connected,&module_fully_connected_plastic,
	&module_flatten,&module_reshape,&module_merger_udp,&module_split_events}; //catalog of functions, as array of funtion pointers

// Functions for parameter extraction from parameter files
typedef int (*functionParamsPointer)(prmss*, char*);
	int module_copy_params();
	int module_conv_params();
	int module_conv_plastic_params();
	int module_stdp_params();
    int module_stdp_orderbased_params();
    int module_fully_connected_params();
    int module_fully_connected_plastic_params();
    int module_flatten_params();
    int module_reshape_params();
    int module_merger_udp_params();
    int module_split_events_params();
	char *FunctionsCatalogParams[] = {"module_copy_params","module_conv_params",
	"module_conv_plastic_params","module_stdp","module_stdp_orderbased_params",
	"module_fully_connected_params","module_fully_connected_plastic_params",
	"module_flatten_params","module_reshape_params","module_merger_udp_params",
	"module_split_events_params"}; //strings with parameter extracting functions of modules
	functionParamsPointer funPtrCatalogParams[] = {&module_copy_params,
	&module_conv_params,&module_conv_plastic_params,&module_stdp_params,
	&module_stdp_orderbased_params,&module_fully_connected_params,
	&module_fully_connected_plastic_params,&module_flatten_params, 
	&module_reshape_params,&module_merger_udp_params,&module_split_events_params}; //catalog of parameter extraction functions

// Functions for initial state extraction from initial state files
typedef int (*functionStatesPointer)(prmss*, char*, prmss*);
	int module_copy_states();
	int module_conv_states();
	int module_conv_plastic_states();
	int module_stdp_states();
    int module_stdp_orderbased_states();
    int module_fully_connected_states();
    int module_fully_connected_plastic_states();
    int module_flatten_states();
    int module_reshape_states();
    int module_merger_udp_states();
    int module_split_events_states();
	char *FunctionsCatalogStates[] = {"module_copy_states","module_conv_states",
	"module_conv_plastic_states","module_stdp_states",
	"module_stdp_orderbased_states","module_fully_connected_states",
	"module_fully_connected_plastic_states","module_flatten_states",
	"module_reshape_states","module_merger_udp_states","module_split_events_states"}; //strings with initial state extracting functions of modules
	functionStatesPointer funPtrCatalogStates[] = {&module_copy_states,
	&module_conv_states,&module_conv_plastic_states,&module_stdp_states,
	&module_stdp_orderbased_states,&module_fully_connected_states,
	&module_fully_connected_plastic_states,&module_flatten_states, 
	&module_reshape_states,&module_merger_udp_states,&module_split_events_states}; //catalog of state extraction functions

#endif
