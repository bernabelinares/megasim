#!/opt/local/bin/python
import pylab as plt
import numpy as np
import sys

def loadKernelFromParamFile(filenames):
    """
    """
    q= open(filenames)
    lines = q.readlines()
    #find begining of kernel (Dy)
    # and end of kernel (crop...)
    foundstopkernel=0
    for i in range(len(lines)):
        if lines[i].split(" ")[0].lower()=="dy":
            kernelStart=i+1
        if (lines[i].split("_")[0].lower()=="crop") and foundstopkernel==0:
            kernelStop=i
            foundstopkernel=1
    kernelLines = lines[kernelStart:kernelStop]
    
    kernel=[]
    splitKernelLines = [x.rstrip().split(" ") for x in kernelLines]
    for i in range(len(kernelLines)):
        tmp = [x for x in splitKernelLines[i] if not x or x!="\n"]
        tmp =[x for x in tmp if x!=""]
        kernel.append(map(int,tmp))
    return np.asarray(kernel)

def kernelPlot(kernelweights):
    """
    """
    fig2=plt.figure(figsize=(3,2))
    ax=fig2.add_subplot(111)
    imm=ax.imshow(kernelweights,interpolation="none")
    plt.colorbar(imm)
    plt.axis("off")
    plt.show()


if __name__=="__main__":
    if len(sys.argv)<2:
        print "Please enter a parameter file"
        sys.exit(1)
    print sys.argv[1]
    kernel = loadKernelFromParamFile(sys.argv[1])
    kernelPlot(kernel)
