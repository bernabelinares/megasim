#!/opt/local/bin/python

import numpy as np
import sys
import os
import datetime

retinaSizeX=128; 
xshift=1
yshift=8
polmask=1
xmask=0xfe
ymask=0x7f00

if len(sys.argv)<2:
    print "please enter an EVS filename"
    sys.exit(1)

CIN = np.genfromtxt(sys.argv[1],delimiter=" ",dtype="int")
s=sys.argv[1].split('.')[0]
print "converting %s to %s"%(sys.argv[1],s+".dat")

x=CIN[:,3]
y=CIN[:,4]
pol=CIN[:,5]
# Ax=x#(retinaSizeX-1)-x
Ax=(retinaSizeX-1)-x
xfinal=Ax<<xshift
# yfinal=((retinaSizeX-1)-y)<<yshift
yfinal=y<<yshift
polfinal = (1-pol)/2

b1=2**16-1<<16
b2=1<<15
vector_allAddr=np.uint32(b1+b2+yfinal+xfinal+polfinal)
vector_allTs=np.uint32(CIN[:,0])

f = open(s+".dat",'w')
tok='#!AER-DAT'
tok2='# This is a raw AE data file - do not edit'
tok3='# Data format is int32 address, int32 timestamp (8 bytes total), repeated for each event'
tok4='# Timestamps tick is 1 us'
tok5='# Created %s'%(datetime.datetime.now())
v=2.0

f.write('%s'%tok)
f.write('%1.1f\r\n'%v)
f.write('%s\r\n'%tok2)
f.write('%s\r\n'%tok3)
f.write('%s\r\n'%tok4)
f.write('%s\r\n'%tok5)

All = np.uint32(np.zeros((2,len(CIN[:,0]))))
All[0] = vector_allAddr
All[1] = vector_allTs
All = np.reshape(np.transpose(All),(1,len(CIN[:,0])+len(CIN)))[0]
All= All.astype(dtype='>i4')
All.tofile(f)
f.close()
