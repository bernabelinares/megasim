#!/usr/bin/python
#/opt/local/bin/python
from __future__ import division
import pylab as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import time
import numpy as np
import sys
KERNELSIZE=7*7
FONTSIZE=6
WMAX=30678337

def loadKernelFromParamFile(filenames):
    """
    """
    global KERNELSIZE
    q= open(filenames)
    lines = q.readlines()
    FIRSTKERNEL=22
    size_kx=int(lines[FIRSTKERNEL].split(" ")[1])
    size_ky=int(lines[FIRSTKERNEL+1].split(" ")[1])

    kernelLines=lines[FIRSTKERNEL+4:FIRSTKERNEL+4+size_ky]
    splitKernelLines = [x.rstrip().split(" ") for x in kernelLines]
    kernel=[]
    for i in range(len(kernelLines)):
        tmp = [x for x in splitKernelLines[i] if x !="" or x!="\n"]
        kernel.append(map(int,tmp))
    KERNELSIZE=size_kx*size_ky
    return np.asarray(kernel)


def get_kernels_and_params(mainschematicfilename):
	'''
	returns a list of kernel filenames and param filename
	'''

	schfile=open(mainschematicfilename,'r')
	schlines=schfile.readlines()
    
	kernel_fnames=[]
	for i in range(len(schlines)):
		tmp=schlines[i].split("{")[0].strip()
		#if tmp=='module_stdp':
                #if tmp=='module_stdp_orderbased':
                tmpstdp=tmp.split("_")
                try:
                    if tmpstdp[1]=="stdp":    
                        tmp2=schlines[i].split("{")[2].split("}")[0]
                        tmp2="node_"+tmp2+".W"
                        kernel_fnames.append(tmp2)
                except (IndexError):
                    pass

	#get param files for init kernel
	param_fnames=[]
	for i in range(len(schlines)):
		tmp=schlines[i].split("{")[0].strip()
		if tmp=='module_conv_plastic':
			tmp2=schlines[i].split("}")[2].strip().split(" ")[0]
			param_fnames.append(tmp2)
	schfile.close()
	return [kernel_fnames,param_fnames]


def interactivePlot(kernels,initial_kernels,kernel_and_params,fig2,simstats,delay):
    """
    """
    tt=1
    for i in range(len(kernels)):

    	ax=fig2.add_subplot(2,len(kernels),tt)
    	#ttl = ax.text(.0, 1.09, ' ', transform = ax.transAxes, va='center')
    	#ttl.set_text("I.")
        ax.set_title("Initial",fontsize=FONTSIZE)
    	imm1=ax.imshow(initial_kernels[i]/WMAX,interpolation="none",cmap=plt.cm.gray)
        if tt!=1:ax.set_axis_off()
        divider=make_axes_locatable(ax)
        cax=divider.append_axes("right", size="2%", pad=0.05)
        cbar=plt.colorbar(imm1,cax=cax)
        cbar.ax.tick_params(labelsize=8) 

    	tt+=1
    	ax=fig2.add_subplot(2,len(kernels),tt)
    	imm=ax.imshow(kernels[i]/WMAX,interpolation="none",cmap=plt.cm.gray)
    	#ttl = ax.text(.0, 1.09, ' ', transform = ax.transAxes, va='center')
    	#ttl.set_text(kernel_and_params[0][i])
        ax.set_title(str(kernel_and_params[0][i]),fontsize=FONTSIZE)
    	#"time_act: %d,\nReal CPU time: %.1f s, Update: %.1f s"%(int(simstats[0]),simstats[1],simstats[2]) ) #str(text/1000)+" s")  
    	divider=make_axes_locatable(ax)
        cax=divider.append_axes("right", size="2%", pad=0.05)
        ax.set_axis_off()
    	cbar=plt.colorbar(imm,cax=cax)
        cbar.ax.tick_params(labelsize=8) 

    	tt+=1

    plt.pause(delay)
    plt.clf()




kernel_and_params= get_kernels_and_params(sys.argv[1])
print kernel_and_params
initial_kernels=[]
kernels=[]

if len(kernel_and_params[0])!=1:
    for i in range(len(kernel_and_params[0])):
        initial_kernels.append(loadKernelFromParamFile(kernel_and_params[1][i]))
        kernels.append(loadKernelFromParamFile(kernel_and_params[1][i]))
else:
    initial_kernels.append(loadKernelFromParamFile(kernel_and_params[1][0]))
    kernels.append(loadKernelFromParamFile(kernel_and_params[1][0]))

plt.ion()
fig2=plt.figure()
plt.subplots_adjust(left=0.05, bottom=0.01, right=0.95, top=0.95, wspace=0.6, hspace=0.3)
interactivePlot(kernels,initial_kernels,kernel_and_params,fig2,simstats=[0,0,0],delay=0.001)
while(True):
	readytoplot=True
	for i in range(len(kernels)):
		try:
			kernels[i]=np.genfromtxt(kernel_and_params[0][i],
        		delimiter=" ",dtype="int")
		except ValueError:
			time.sleep(0.1) 
			continue
		except IOError:
			time.sleep(0.1) 
			continue
	for i in range(len(kernels)):
		if kernels[i].size<KERNELSIZE:
			readytoplot=False
			time.sleep(0.1) 
			continue
	if readytoplot:	
		interactivePlot(kernels,initial_kernels,kernel_and_params,fig2,simstats=[0,0,0],delay=0.001)
        time.sleep(0.1) 