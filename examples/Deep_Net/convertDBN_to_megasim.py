"""
Created on Fri Mar 21 20:13:59 2014

@author: Vagoir
"""

import edbnTools.edbnTools as et

#import pylab as plt
import numpy as np
import time 
import sys
import scipy.io as sio

MAXINT= 2**30

thres=1.0
taum=5000 # ms
tauref = 2 # ms


TRAINEDEDBN = 'edbn_95.52.mat'
#spiNNakerPackageFolder = '/Users/Vagoir/Programming/SpiNNaker/SpiNNaker_package_EDBN/repo/EDBNSpiNNaker/EDBN_SPINNAKER'

# Read the topology and weights of the Event-based trained DBN
topology, weights=et.loadMatEDBNDescription(TRAINEDEDBN)

max_min_w=[]
for l in weights:
	max_min_w.append([max(l.flatten()),min(l.flatten())])

largest_value_is = np.max(np.abs(np.asarray(max_min_w).flatten()))
new_thres = ((float(thres)/largest_value_is)*MAXINT).astype("int")

new_weights=[]
for i,w in enumerate(weights):
	new_weights.append(
		((w.transpose()/largest_value_is)*MAXINT).astype("int")
		)
	np.savetxt("w"+str(i),new_weights[i],delimiter=" ",fmt="%d")

print "Threshold is %d, refractory %d, tau_m %d"%(new_thres, tauref, new_thres/taum)

