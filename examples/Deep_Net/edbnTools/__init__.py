#
"""
    Created on Tue Mar 18 16:20:13 2014
    
    A set of tools for the event-based deep belief networks on SpiNNaker
    
    Some functions were transformed from MATLAB code given to me by Daniel O'Neil
    University of Zurich
    
    PYTHON  version  2.7.6
    NUMPY   version  1.0.8
    SCIPY   version  0.13.1
    PYLAB   version
    
    SpiNNaker Package version
    Scamp version  -  1.09 (Change to 200 MHz)
    ybug version
    
    @author: Vagoir
"""
