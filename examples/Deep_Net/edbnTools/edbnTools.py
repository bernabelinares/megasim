# -*- coding: utf-8 -*-
"""
Created on Tue Mar 18 16:20:13 2014

A set of tools for the event-based deep belief networks on SpiNNaker

Some functions were transformed from MATLAB code given to me by Daniel O'Neil
University of Zurich

PYTHON version
NUMPY version
SCIPY version
PYLAB version

SpiNNaker Package version
Scamp version  -  1.09 (Change to 200 MHz)
ybug version

@author: Vagoir
"""

import scipy.io as sio
import pylab as plt
import matplotlib.cm as cm
import numpy as np

def loadMatMNISTMatlab(pathtoMNISTMAT):
    '''
    This function opens mnist_uint8.mat which holds the MNIST dataset as in Dan's Matlab
    implementation
    '''
    openMatMNIST = sio.loadmat(pathtoMNISTMAT)
    MNIST = openMatMNIST['test_x'] / 255.0 * 0.2
    MLABELS = openMatMNIST['test_y'] / 255.0 * 0.2
    return MNIST, MLABELS


def loadMatMNIST(pathtoMNISTMAT):
    '''
    USE loadMatMNISTMatlab(pathtoMNISTMAT) instead of this

    This function opens a MAT file that contains the MNIST Handwritten testset. This
    file has a structure ('mnist_testset') with two fields, the first one ('im') contains
    10,000 test characters of the MNIST dataset, while the second one ('labs')
    their labels. The MATLAB files of the MNIST dataset were provided from
    Daniel o'Neil, University of Zurich, Institute of NeuroInformatics (INI).

    Returns a 28 x 28 x 10,000 NUMPY array that holds all the test set, and a
    string array with the 10,000 labels.
    '''
    openMatMNIST = sio.loadmat(pathtoMNISTMAT)

    MNIST   =   openMatMNIST['mnist_testset'][0][0][0] # 3d matrix, x, y, digit
    MLABELS =   openMatMNIST['mnist_testset'][0][0][1][0]

    return MNIST,MLABELS


def loadMatEDBNDescription(matfile):
    '''
        This function opens a MAT description of a trainned Event-Based
        Deep Belief Network and returns the number of neurons in each layer of the network
        and a list of NUMPY arrays of the synaptic weights of each layer.

        Returns: A numpy vector of the DBN's topology.
                 A list of numpy matrices holding the weights of each layer.
        Note:
        When opening a EDBN MATLAB file using SCIPY.IO keep this in mind:
        To access each cell of 'erbm' use the following instruction
        edbn['edbn']['erbm'][0][0][0][ERBM_CELL_NUMBER][0][0][FIELD])

        Where field is:
        [0]  -> alpha
        [1]  -> decay
        [2]  -> momentum
        [3]  -> f_infl
        [4]  -> f_decay
        [5]  -> f_alpha
        [6]  -> pcd
        [7]  -> sp
        [8]  -> sp_infl
        [9]  -> sieg (struct)
        [10] ->  W
        [11] -> b
        [12] -> c
        [13] -> vW
        [14] ->vb
        [15] -> vc
        [16] -> cache (struct)
        '''
    _WEIGHTStr = 10 # Weights for each layer are saved here
    edbn = sio.loadmat(matfile)
    edbnTopology =  edbn['edbn']['sizes'][0][0][0] #number of layers and neurons
    # Get Weights for each layer
    weightList = []
    for l in range(edbnTopology.size-1):
        weightList.append(edbn['edbn']['erbm'][0][0][0][l][0][0][_WEIGHTStr])
    return edbnTopology, weightList



def convertMNISTtoSpikeTrains(mnist,n_spikes=2000,t_stop=2,asInMatlab=False):
    '''
    This function receives an MNIST character, 28 x 28 array, as input and
    generates random spikes based on the intensity of the pixels, the number of
    spikes desired (n_spikes) and the duration of the stimulus (t_stop).

    Returns a NUMPY 2D array, where the first column holds the neuron IDs (addresses)
    and second column the spike-times (timestamps).

    Based on Daniel O'Neil's MATLAB scripts
    '''
    #y = randsample(n,k,true,w) or y = randsample(population,k,true,w)
    #returns a weighted sample taken with replacement, using a vector of
    #positive weights w, whose length is n. The probability that the integer
    #i is selected for an entry of y is w(i)/sum(w). Usually, w is a vector of
    #probabilities. randsample does not support weighted sampling without
    #replacement.

    # Following the Matlab implementation where the characters
    # are fliped horizontally and rotated by 90 dec counter clockwise
    dt       = 1 / 1000.0 # spikes with ms resolutions
    if asInMatlab:
        reshapeCh = mnist.reshape(mnist.size,order='F')
    else:
        reshapeCh = mnist.reshape(mnist.size)
    spikesAdr = np.random.choice(len(reshapeCh),n_spikes, True,
                                 (reshapeCh/sum(reshapeCh)) )
    spikesTs = np.round(np.sort(np.random.random(len(spikesAdr))
                                *t_stop)/dt) # in ms
    Spikes=np.concatenate((spikesAdr,spikesTs),axis=-1)
    Spikes=Spikes.reshape((len(spikesAdr),2),order='F')
    return Spikes

def convertMNISTtoSpikeTrainsMatlab(mnist,n_spikes=2000,t_stop=2):
    '''
    This function receives an MNIST character, 28 x 28 array, as input and
    generates random spikes based on the intensity of the pixels, the number of
    spikes desired (n_spikes) and the duration of the stimulus (t_stop).

    Returns a NUMPY 2D array, where the first column holds the neuron IDs (addresses)
    and second column the spike-times (timestamps).

    Based on Daniel O'Neil's MATLAB scripts
    '''
    #y = randsample(n,k,true,w) or y = randsample(population,k,true,w)
    #returns a weighted sample taken with replacement, using a vector of
    #positive weights w, whose length is n. The probability that the integer
    #i is selected for an entry of y is w(i)/sum(w). Usually, w is a vector of
    #probabilities. randsample does not support weighted sampling without
    #replacement.

    # Following the Matlab implementation where the characters
    # are fliped horizontally and rotated by 90 dec counter clockwise
    dt       = 1 / 1000.0 # spikes with ms resolutions
    spikesAdr = np.random.choice(len(mnist),n_spikes, True,
                                 (mnist/sum(mnist)) )
    spikesTs = np.round(np.sort(np.random.random(len(spikesAdr))
                                *t_stop)/dt) # in ms
    Spikes=np.concatenate((spikesAdr,spikesTs),axis=-1)
    Spikes=Spikes.reshape((len(spikesAdr),2),order='F')
    return Spikes


def sortMembranebyTimeSpiNNaker(spinnakerMembrane):
    '''
    This simple method converts a list of membrance traces received from spiNNaker to
    a pyNN-style format.
    TODO: Probably I need to add this to the SpiNNaker Package.
    '''
    memArray = (spinnakerMembrane)
    memIndex = np.lexsort((memArray[:,0],memArray[:,1]))
    memArraySorted = memArray[memIndex]
    return memArraySorted

