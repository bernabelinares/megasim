import numpy as np
import brian as br

class BrianSim():
    """
    Use brian as as simulator for spiking deep belief networks

    TODO: Set the topology once
    """
    # Default Sim Params
    def_clock = 1 * br.ms # milisecond time-step
    runFor = 1 * br.second # run for 2 seconds

    # Default Neuron Params
    tau_m = 5000000 * br.ms # membrane time constant
    v_r = 0 * br.mV # reset potential
    v_th = 1000 * br.mV # threshold potential
    ref = 2.0 * br.ms # refractory period

    # Default Neuron Model
    neuronModel = '''
        dv/dt = -v/params['tau_m'] : volt
        '''

    def __init__(self):
        pass

    def setNeuronParams(self,neuronParams):
        self.tau_m = neuronParams['tau_m'] * br.ms
        self.v_r = neuronParams['v_r'] * br.mV
        self.v_th = neuronParams['v_th'] * br.mV
        self.ref = neuronParams['refractoryPeriod'] * br.ms

    def setNeuronModel(self,eq):
        self.neuronModel = eq

    def setSimulationTime(self,runfor):
        """
        Set the simulation stop time is seconds, default value = 2
        """
        self.runFor = runfor*br.second

    def loadWeights(self,weights):
        self.weightlist = weights


    def modifySpikeTimes(self,spiketimes):
        """
        Brian wants spike-times to be in a millisecond time scale
        """
        spiketimes[:,1] = spiketimes[:,1] * br.ms
        return spiketimes

    def convertSpikes(self, brianRecSpikes):
        """
        This method converts the spike-times recorded from brian to
        a pyNN compatible format. The array is sorted based on the spike-times

        Returns: A 2D numpy array where the first column holds the neuron id,
                 and the second column the spike-times in milliseconds.
        """
        sp = []
        for neuron in brianRecSpikes.keys():
            for i in range(len(brianRecSpikes[neuron])):
                sp.append([neuron,round(brianRecSpikes[neuron][i]*1000)])
        spiketimes = np.asarray(sp)
        try:
            spIndex = np.lexsort((spiketimes[:,0],spiketimes[:,1]))
            spArraySorted = spiketimes[spIndex]
        except(IndexError):
            spArraySorted=spiketimes
        return spArraySorted

    def runSim(self,inputSpikes,topology,weights):
        """
        Returns: A numpy array with the spike counts.
                 A 2D numpy array with the first column holding the
                 neuron id and the second one the spike-times (ms). The
                 array is sorted based on the spike-times.
        """
        br.defaultclock.dt = self.def_clock
        br.reinit_default_clock()
        br.clear(True)

        br.set_global_preferences(useweave=False)#gcc_options = ['-O3'])

        pops = []
        conns = []

        params ={}
        params['tau_m'] = self.tau_m # membrane time constant
        params['v_r'] = self.v_r # reset potential
        params['v_th'] = self.v_th # threshold potential
        params['eqs'] = self.neuronModel

        inputSpikes = self.modifySpikeTimes(inputSpikes)

        # Build populations
        for (layer_num, layer_size) in enumerate(topology):
            if layer_num == 0:
                pops.append(br.SpikeGeneratorGroup(layer_size, inputSpikes))
            else:
                pops.append(br.NeuronGroup(layer_size, model=self.neuronModel, threshold=self.v_th, reset=self.v_r,refractory=self.ref))

        # Build connections
        for (layer_num, weights) in enumerate(weights):
            c = br.Connection(pops[layer_num], pops[layer_num+1], 'v', weight=weights.T*1000*br.mV)
            conns.append(c)

        # Record the output population
        out_spikes_count = br.SpikeCounter(pops[-1])

        out_raster_spikes = br.SpikeMonitor(pops[-1])

        # run forest run
        br.run(self.runFor)

        # Convert spike-trains to a pyNN compatible format
        out_raster_spikes = self.convertSpikes(out_raster_spikes.spiketimes)

        # returns a brian SpikeCount object
        return out_spikes_count.count,out_raster_spikes


