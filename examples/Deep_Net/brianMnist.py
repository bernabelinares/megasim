# -*- coding: utf-8 -*-
"""
Created on Fri Mar 21 20:13:59 2014

@author: Vagoir
"""

import edbnTools.edbnTools as et
import edbnTools.simulators.brianSimulator as edbnBrian

#import pylab as plt
import numpy as np
import time 
import sys
import scipy.io as sio


TRAINEDEDBN = 'edbn_95.52.mat'
#spiNNakerPackageFolder = '/Users/Vagoir/Programming/SpiNNaker/SpiNNaker_package_EDBN/repo/EDBNSpiNNaker/EDBN_SPINNAKER'


if __name__ == '__main__':
    # Simulation run time in ms    
    runtime = 1000
    
    # Open MAT file of the MNIST Dataset
    mnist, labels = et.loadMatMNISTMatlab('mnist_uint8.mat')

    # Read the topology and weights of the Event-based trained DBN
    topology, weights=et.loadMatEDBNDescription(TRAINEDEDBN)

    # Traverse throught the whole set

    brSim = edbnBrian.BrianSim()
    #np.random.seed(seed=7)   
    results=[]
    total_elapsed_time=[]
    total_cpu_time = []
    output_hist = []
    output_classified = []

    for digit in range(10000):
        np.random.seed(seed=7)
        print "Running digit %d"%(digit)
        # Convert that digit to the spiking domain
        mnistSpikes=et.convertMNISTtoSpikeTrainsMatlab(mnist[digit],n_spikes=2000,t_stop=1)
        
   
        tic=time.time()
        out,outsp = brSim.runSim(mnistSpikes,topology,weights)
        toc=time.time()-tic

        results.append(np.argmax(out) == np.argmax(labels[digit]))
        output_hist.append(out)
        output_classified.append(np.argmax(out))
        total_elapsed_time.append(toc)

    print "correctly guessed digits %d"%(np.sum(results))
