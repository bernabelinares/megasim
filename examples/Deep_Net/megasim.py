# -*- coding: utf-8 -*-
"""
Created on Fri Mar 21 20:13:59 2014

@author: Vagoir
"""

import numpy as np
import scipy.io as sio
import os 



def loadMatMNISTMatlab(pathtoMNISTMAT):
    '''
    This function opens mnist_uint8.mat which holds the MNIST dataset as in Dan's Matlab
    implementation
    '''
    openMatMNIST = sio.loadmat(pathtoMNISTMAT)
    MNIST = openMatMNIST['test_x'] / 255.0 * 0.2
    MLABELS = openMatMNIST['test_y'] / 255.0 * 0.2
    return MNIST, MLABELS

def convertMNISTtoSpikeTrainsMatlab(mnist,n_spikes=2000,t_stop=2):
    '''
    This function receives an MNIST character, 28 x 28 array, as input and
    generates random spikes based on the intensity of the pixels, the number of
    spikes desired (n_spikes) and the duration of the stimulus (t_stop).

    Returns a NUMPY 2D array, where the first column holds the neuron IDs (addresses)
    and second column the spike-times (timestamps).

    Based on Daniel O'Neil's MATLAB scripts
    '''
    #y = randsample(n,k,true,w) or y = randsample(population,k,true,w)
    #returns a weighted sample taken with replacement, using a vector of
    #positive weights w, whose length is n. The probability that the integer
    #i is selected for an entry of y is w(i)/sum(w). Usually, w is a vector of
    #probabilities. randsample does not support weighted sampling without
    #replacement.

    # Following the Matlab implementation where the characters
    # are fliped horizontally and rotated by 90 dec counter clockwise
    dt       = 1 / 1000.0 # spikes with ms resolutions
    spikesAdr = np.random.choice(len(mnist),n_spikes, True,
                                 (mnist/sum(mnist)) )
    spikesTs = np.round(np.sort(np.random.random(len(spikesAdr))
                                *t_stop)/dt) # in ms
    Spikes=np.concatenate((spikesAdr,spikesTs),axis=-1)
    Spikes=Spikes.reshape((len(spikesAdr),2),order='F')
    return Spikes

def convert_to_megasim(pynn_spikes,inputsize=28):
    megastim = np.zeros((len(pynn_spikes),6),dtype="int" )
    # copy time stamps
    megastim[:,0] = pynn_spikes[:,1]
    megastim[:,1] = np.ones( (len(pynn_spikes))) *-1
    megastim[:,2] = np.ones( (len(pynn_spikes))) *-1
    megastim[:,3] = pynn_spikes[:,0]% inputsize
    megastim[:,4] = pynn_spikes[:,0]/ inputsize#sensorSize-ys# in megasim Ys are inverted
    megastim[:,5] = np.ones( (len(pynn_spikes)))

    return megastim


TRAINEDEDBN = '97.edbn_95.52.mat'


if __name__ == '__main__':
    # Simulation run time in ms    
    runtime = 1000
    
    # Open MAT file of the MNIST Dataset
    mnist, labels = loadMatMNISTMatlab('mnist_uint8.mat')

    total_elapsed_time=[]
    total_cpu_time = []
    output_classified = []

    # check if files exist
    try:
        d= np.loadtxt("current_digit.log",dtype="int")
        d+=1
        output_hist = np.genfromtxt("outputspikeshistogram.log",delimiter=" ",dtype="int")
        output_hist = list(output_hist)
    except (IOError):
        d=0
        output_hist=[]

    for dig in range(d,10000):
        np.random.seed(seed=7)
        #print "Running digit %d"%(dig)
        mnistSpikes=convertMNISTtoSpikeTrainsMatlab(mnist[dig],n_spikes=2000,t_stop=1)
        megaSpikes = convert_to_megasim(mnistSpikes,inputsize=28)
        
        labelSpikes = convertMNISTtoSpikeTrainsMatlab(labels[dig],n_spikes=2000,t_stop=1)
        labelMegaSpikes = convert_to_megasim(labelSpikes,inputsize=10)

        np.savetxt("mnist_digit.txt",megaSpikes,delimiter=" ",fmt="%d")
        np.savetxt("mnist_lbl_digit.txt",labelMegaSpikes,delimiter=" ",fmt="%d")

        #run network
        run_megasim= os.popen("../../bin/megasim dbn2hlayers.sch").read()

        total_elapsed_time.append(
            float(run_megasim.splitlines()[-4].split(":")[1].split(" ")[1])
            )

        total_cpu_time.append( 
             float(run_megasim.splitlines()[-5].split(":")[1].split(" ")[1])
            )

        outspikes =np.genfromtxt("node_out.evs",delimiter=" ",dtype=("int"))
        outputhist = np.histogram(outspikes[:,3],bins=10,range=(0,10))[0]
        digitis =  np.argmax(outputhist)
        output_classified.append(digitis)
        output_hist.append(outputhist)
        print "Current index is %d, Label:%d, MegaSim:%d, Status:%d"%(dig, np.argmax(labels[dig]),output_classified[-1],np.argmax(labels[dig])==output_classified[-1])
        
        # Store results because I've been getting
        tmp=np.asarray(output_hist)
        np.savetxt("outputspikeshistogram.log",tmp,delimiter=" ",fmt="%d")

        f=open("current_digit.log","w")
        f.write(str(dig))
        #      np.savetxt("current_digit.log",dig)
        f.close()
    tr=[np.argmax(x) for x in labels[:len(output_classified)]]
    p=[]
    for i in range(len(tr)):
        p.append(output_classified[i]==tr[i])
    print "correctly guessed digits %d"%(np.sum(p))

