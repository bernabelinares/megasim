.netlist
source {mnist} mnist_digit.txt
source {mlabels} mnist_lbl_digit.txt

module_flatten {mnist}{in0} flatten_params.prm init_state.stt
module_fully_connected {in0} {l1} fully_con1.prm init_state.stt
module_fully_connected {l1} {l2} fully_con2.prm init_state.stt
module_fully_connected {l2} {out} fully_con_out.prm init_state.stt

module_fully_connected {l1} {reconstrf} fully_con_recon.prm init_state.stt
module_reshape {reconstrf}{recon} reshaperec_params.prm init_state.stt

module_reshape {l1}{h1_reshape} reshapeh1_params.prm init_state.stt
module_reshape {l2}{h2_reshape} reshapeh1_params.prm init_state.stt

module_merger_udp {mnist,h1_reshape,h2_reshape,out,mlabels,recon}{merged} merger_udp.prm init_state.stt

.options
Tmax = 429290101
