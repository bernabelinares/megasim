from __future__ import division
import pylab as plt
import numpy as np
import time
import pdb

KERNELSIZE=7*7
count=0

def loadKernelFromParamFile(filenames):
    """
    """
    q= open(filenames)
    lines = q.readlines()
    #find begining of kernel (Dy)
    # and end of kernel (crop...)
#    foundstopkernel=0
#    for i in range(len(lines)):
#        if lines[i].split(" ")[0].lower()=="dy":
#            kernelStart=i+1
#        if (lines[i].split("_")[0].lower()=="crop") and foundstopkernel==0:
#            kernelStop=i
#            foundstopkernel=1
#    kernelLines = lines[kernelStart:kernelStop]
#    
#    kernel=[]
    FIRSTKERNEL=22
    size_kx=int(lines[FIRSTKERNEL].split(" ")[1])
    size_ky=int(lines[FIRSTKERNEL+1].split(" ")[1])

    kernelLines=lines[FIRSTKERNEL+4:FIRSTKERNEL+4+size_ky]
    splitKernelLines = [x.rstrip().split(" ") for x in kernelLines]
    kernel=[]
    for i in range(len(kernelLines)):
        tmp = [x for x in splitKernelLines[i] if x !="" or x!="\n"]
        kernel.append(map(int,tmp))
    return np.asarray(kernel)


fig2=plt.figure(figsize=(13,8))
initialKernel = loadKernelFromParamFile("stdp_conv_postI1.prm")
initialKernel2 = loadKernelFromParamFile("stdp_conv_postI2.prm")


try:  
    w=np.genfromtxt("node_dw1.W",delimiter=" ",dtype="int")
except (IOError):
    w=initialKernel
try:
    w2=np.genfromtxt("node_dw2.W",delimiter=" ",dtype="int")
except (IOError):
    w2=initialKernel2

try:  
    dw1=np.genfromtxt("node_dw1.evs",delimiter=" ",dtype="int")
except (IOError):
    dw1=np.zeros((6,6))
try:
    dw2=np.genfromtxt("node_dw2.evs",delimiter=" ",dtype="int")
except (IOError):
    dw2=np.zeros((6,6))


dw1histp=np.zeros((7,7))
dw1histp_id=np.where(dw1[:,5]>0)[0]
dw1p=dw1[dw1histp_id,:]

dw1histn=np.zeros((7,7))
dw1histn_id=np.where(dw1[:,5]<0)[0]
dw1n=dw1[dw1histn_id,:]

dw2histp=np.zeros((7,7))
dw2histp_id=np.where(dw2[:,5]>0)[0]
dw2p=dw2[dw2histp_id,:]

dw2histn=np.zeros((7,7))
dw2histn_id=np.where(dw2[:,5]<0)[0]
dw2n=dw2[dw2histn_id,:]

for i in range(len(dw1p)):
    dw1histp[dw1p[i,4],dw1p[i,3]]+=dw1p[i,5]

for i in range(len(dw1n)):
    dw1histn[dw1n[i,4],dw1n[i,3]]+=dw1n[i,5]

for i in range(len(dw2p)):
    dw2histp[dw2p[i,4],dw2p[i,3]]+=dw2p[i,5]

for i in range(len(dw2n)):
    dw2histn[dw2n[i,4],dw2n[i,3]]+=dw2n[i,5]

sim_status=np.genfromtxt("megasim_progress.tmp",delimiter=" ")
ax=fig2.add_subplot(2,5,1)
imm1=ax.imshow(initialKernel,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imm1)
ax.set_title("Initial kernel")

ax=fig2.add_subplot(2,5,2)
imm11=ax.imshow(w,interpolation="none",cmap=plt.cm.gray)
ttl = ax.text(.0, 1.09, ' ', transform = ax.transAxes, va='center')
plt.colorbar(imm11)
ax.set_title("Final kernel")

ax=fig2.add_subplot(2,5,3)
imdw1hist=ax.hist(initialKernel.flatten(),bins=10,range=(0,30678337),color='r',alpha=0.5) 
imdw1hist=ax.hist(w.flatten(),bins=10,range=(0,30678337),color='b',alpha=0.5) 
ax.set_xlim(0,30678337)
ax.set_title("Initial and \nfinal distribution")

ax=fig2.add_subplot(2,5,4)
imdw1=ax.imshow(dw1histp,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imdw1)
ax.set_title("DW events (+)")

ax=fig2.add_subplot(2,5,5)
imdw1=ax.imshow(dw1histn,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imdw1)
ax.set_title("DW events (-)")

ax=fig2.add_subplot(2,5,6)
imm2=ax.imshow(initialKernel2,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imm2)
ax=fig2.add_subplot(2,5,7)
imm22=ax.imshow(w2,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imm22)
ttl = ax.text(.0, 1.09, ' ', transform = ax.transAxes, va='center')

ax=fig2.add_subplot(2,5,8)
imdw1hist=ax.hist(initialKernel2.flatten(),bins=10,range=(0,30678337),color='r',alpha=0.5) 
imdw2hist=ax.hist(w2.flatten(),bins=10,range=(0,30678337),color='b',alpha=0.5) 
ax.set_xlim(0,30678337)

ax=fig2.add_subplot(2,5,9)
imdw1=ax.imshow(dw2histp,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imdw1)
ax.set_title("DW events (+)")

ax=fig2.add_subplot(2,5,10)
imdw1=ax.imshow(dw2histn,interpolation="none",cmap=plt.cm.gray)
plt.colorbar(imdw1)
ax.set_title("DW events (-)")

plt.show()

