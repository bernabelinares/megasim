This example shows a single plastic convolutional module with self inhibitory connections learning a synthetically generated symbol. Two STDP modules are used: the order-based STDP [Rocklin et al. 2013] and the default STDP module [Song et al. 2000]

run: 
megasim multiple_plastic_convlayer_orderbased_stdp.sch to use the order-based STDP module
or
megasim multiple_plastic_convlayer_stdp for the STDP module

to plot the weights interactively type: plot_evolution.py [netlist file]

Description of the Files:
nmoving_plus_nnoise.stim - non moving symbol, no background noise
moving_plus_nnoise.stim - moving symbol, no background noise
nmoving_plus_wnoise.stim - non moving symbol, with background noise
moving_plus_wnoise.stim - moving symbol, with background noise
purenoise.stim - No input symbols, only background noise

single_plastic_convlayer_orderbased_stdp.sch - MegaSim netlist file that uses the order-based STDP module 
single_plastic_convlayer_stdp.sch - MegaSim netlist file that uses the STDP module
stdp_orderbased.prm - Parameters of the order-based STDP module
stdp_doubleexp.prm - parameters for the STDP module (double exponential)
stdp_flat.prm - parameters for the STDP module (rectangular)
stdp_conv_postI*.prm - Parameters of the plastic convolutional layers
simple1.stt - Initial states of the MegaSim modules
clear.sh - removes the MegaSim generated files 
merge_and_convert_to_jaer.sh - converts and merges the output spikes to jAER format

References:
D. Roclin, O. Bichler, C. Gamrat, S. J. Thorpe, and J.-O. Klein, “Design study of efficient digital order-based STDP neuron implementations for extracting temporal features,” 2013 Int. Jt. Conf. Neural Networks, pp. 1–7, Aug. 2013.
S. Song, K. D. Miller, and L. F. Abbott, “Competitive Hebbian learning through spike-timing-dependent synaptic plasticity.,” Nat. Neurosci., vol. 3, no. 9, pp. 919–26, Sep. 2000.