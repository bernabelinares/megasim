from __future__ import division
import pylab as plt
import numpy as np
import time

NUM_PRE_NEURONS=1000

def interactivePlot(normweights,fig2,simstats,delay):
    """
    """
    #fig2=plt.figure()
    #ax=fig2.add_subplot(131)
    #ax.hist(INITIAL_W[0,:]/max(INITIAL_W[0,:]),bins=20,range=(0,1))
    #ax.set_xlabel("Normalised weights")
    #ax.set_ylim((0,300))
    #ax.hlines(np.mean(INITIAL_W[0,:]),imseNeuron.Alowest,imseNeuron.Ahighest,color='r')
    ax=fig2.add_subplot(121)
    ax.hist(normweights,bins=20,range=(0,1))
    ax.set_ylim((0,450))
    ax.set_xlabel("Normalised weights")
    ttl0 = ax.text(.2, 1.05, 'MegaSim STDP', transform = ax.transAxes, va='center')
    #ax.hlines(np.mean(INITIAL_W[0,:]),imseNeuron.Alowest,imseNeuron.Ahighest,color='r')
    ax=fig2.add_subplot(122)
    ax.scatter(range(0,NUM_PRE_NEURONS),normweights)
    ax.set_ylim((0,1))
    ax.set_xlabel("pre-synaptic neuron ID")
    ttl = ax.text(.0, 1.05, ' ', transform = ax.transAxes, va='center')
    ttl.set_text("time_act: %d,\nReal CPU time: %.1f s, Update: %.1f s"%(int(simstats[0]),simstats[1],simstats[2]) ) #str(text/1000)+" s")  

    plt.pause(delay)
    plt.clf()

INTERACTIVE_PLOT=True
if INTERACTIVE_PLOT: 
    plt.ion()
    fig2=plt.figure()

while(True):
    try:
        w=np.genfromtxt("node_dw1.W",delimiter=" ",dtype="int")
	sim_status=np.genfromtxt("megasim_progress.tmp",delimiter=" ")
    except ValueError:
        time.sleep(0.1) 
        continue
    except IOError:
        time.sleep(0.1) 
        continue
    w=w.flatten()
    try:
        normweights=w/max(w)
    except ValueError:
        time.sleep(0.1) 
        continue
    if normweights.size<NUM_PRE_NEURONS:
        time.sleep(0.1) 
        continue
    interactivePlot(normweights,fig2,simstats=sim_status,delay=0.001)
