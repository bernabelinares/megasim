import numpy as np

def generatePoissonianSpikeTrain(firingRatePerNeuron,populationSize,durationStimulus,timeStep=1e-3,):
    totalSpikes = durationStimulus*populationSize*firingRatePerNeuron
    mypopFR=np.ones(populationSize)

    spikes=convertImagetoSpikeTrains(mypopFR,totalSpikes,durationStimulus,timeStep)#ms steps
    megaspikes=convertSpikeTrainstoMEGASIMStimulus(spikes,sensorSize=populationSize)
    return megaspikes

def convertImagetoSpikeTrains(imageArray,n_spikes,t_stop=1,dt=1/1000000.0):
    '''

    '''

    reshapeCh = imageArray.reshape(imageArray.size)
    spikesAdr = np.random.choice(len(reshapeCh),n_spikes, True,
                                 (reshapeCh/sum(reshapeCh)) )
    spikesTs = np.round(np.sort(np.random.random(len(spikesAdr))
                                *t_stop)/dt) # in ms
    Spikes=np.concatenate((spikesAdr,spikesTs),axis=-1)
    Spikes=Spikes.reshape((len(spikesAdr),2),order='F')
    return Spikes
    
def convertSpikeTrainstoMEGASIMStimulus(spikeImage,sensorSize=128):
    """
    """
    megastim = np.zeros((len(spikeImage),6),dtype="int" )
    # copy time stamps
    megastim[:,0] = spikeImage[:,1]
    megastim[:,1] = np.ones( (len(spikeImage))) *-1
    megastim[:,2] = np.ones( (len(spikeImage))) *-1
    # compute y addresses 
    # not good python programming here! this is slow
    for i in range(len(spikeImage)):
        ys = int(spikeImage[i,0]/sensorSize)
        xs = int(spikeImage[i,0] - (ys*sensorSize))
        megastim[i,3] = xs
        megastim[i,4] = ys#sensorSize-ys# in megasim Ys are inverted
    
    # all polarity =1 
    megastim[:,5] = np.ones( (len(spikeImage)))

    return megastim


np.random.seed(28374)
NUM_POST_NEURONS = 1
NUM_PRE_NEURONS = 1000
frate=10 # hz
durStimulus=1200 # seconds
print "Generating spikes: firing rate %d Hz, duration of stimulus %d s"%(frate,durStimulus)
spikes = generatePoissonianSpikeTrain(frate,NUM_PRE_NEURONS,durStimulus,timeStep=1e-6)
ind=np.argmax(spikes[:,0]>0)
spikes = spikes[ind:]
np.savetxt("prespikes.stim",spikes,delimiter=" ",fmt="%d")
