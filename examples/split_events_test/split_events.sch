.netlist

source {in} cardsin_randtype.stim

module_split_events {in,in},{pos_ev,neg_ev} params_splitter.prm init_state.stt

module_merger_udp {in,pos_ev,neg_ev}{merged} merger_udp.prm init_state.stt

.options
Tmax=950171000
