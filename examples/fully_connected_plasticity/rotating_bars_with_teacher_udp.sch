.netlist

source {pre} orientationbars.stim
source {teacher} teacher.stim

module_flatten {pre},{flatten} merger_params.prm init_state.stt
module_fully_connected_plastic {flatten,post,teacher,dw1},{post} fc_plastic_teacher.prm init_state.stt

module_stdp {flatten,post}{dw1} stdp_fc.prm init_state.stt

module_merger_udp {pre,teacher,post}{merged} merger_udp.prm init_state.stt

.options
Tmax=9598995
