import numpy as np
import pylab as plt
import scipy.ndimage as sc

#------------------------------------------------
# Simulation Parameters
#------------------------------------------------
#------------------------------------------------
nNeurons = 4 # output neurons
inputsize=16
repeatInputPattern = 4*100*12

n_spikes = 100 #number of spiker
t_stop = 1 # for x seconds
presentNextStimulusAfter = t_stop * 2000.0 #ms


withDecay = False # if true epsilon is set to 0.02
saveResults = True # if true it will save results in the results folder
simTime = t_stop*1000  *repeatInputPattern#ms
nNeurons = 4 # output neurons
#rngseed  = 98766987 # for the random weights and delays, works fine
rngseed  = 1 # for the random weights and delays


def getFiringRates(spikes,nNeurons,presentNextStimulusAfter,simTime):
    firingHist = []
    timeSortedSpikes = sortSpikesbyTime(spikes)
    for tslice in range( int(simTime/presentNextStimulusAfter) ):
        tmp  = np.where((timeSortedSpikes[:,1]<presentNextStimulusAfter)&(timeSortedSpikes[:,1]>-1))
        spikesInThatTimeSlice = timeSortedSpikes[tmp[0][0]:tmp[0][-1]]
        firingHist.append(np.histogram(spikesInThatTimeSlice,range(nNeurons+1))[0])
        timeSortedSpikes[:,1]=timeSortedSpikes[:,1]-presentNextStimulusAfter # move to the next tslice
    return np.asarray(firingHist)

def sortSpikesbyTime(spinnakerMembrane):
    '''
    This simple method converts a list of membrance traces received from spiNNaker to
    a pyNN-style format.
    TODO: Probably I need to add this to the SpiNNaker Package.
    '''
    memArray = (spinnakerMembrane)
    memIndex = np.lexsort((memArray[:,0],memArray[:,1]))
    memArraySorted = memArray[memIndex]
    return memArraySorted


def convertMNISTtoSpikeTrains(mnist,n_spikes=1000,t_stop=2,asInMatlab=False):
    '''
    This function receives a square sized image, as input and
    generates random spikes based on the intensity of the pixels, the number of
    spikes desired (n_spikes) and the duration of the stimulus (t_stop).

    Returns a NUMPY 2D array, where the first column holds the neuron IDs (addresses)
    and second column the spike-times (timestamps).

    '''
    # Following the Matlab implementation where the characters
    # are fliped horizontally and rotated by 90 dec counter clockwise
    dt       = 1 / 1000.0 # spikes with ms resolutions
    if asInMatlab:
        reshapeCh = mnist.reshape(mnist.size,order='F')
    else:
        reshapeCh = mnist.reshape(mnist.size)
    spikesAdr = np.random.choice(len(reshapeCh),n_spikes, True,
                                 (reshapeCh/sum(reshapeCh)) )
    spikesTs = np.round(np.sort(np.random.random(len(spikesAdr))
                                *t_stop)/dt) # in ms
    Spikes=np.concatenate((spikesAdr,spikesTs),axis=-1)
    Spikes=Spikes.reshape((len(spikesAdr),2),order='F')
    return Spikes

def plotRecordedSpikesAsImage(inSpikes,size_in=16,rotate_by=0,flip_Horizontal=False):
    '''
        This function plots a size_in X size_in image (histogram) of the
        spikes received. It expects a 2D array, where first column holds the neuron IDs (Addresses)
        and the second column the spike-times (timestamps). The user can rotate the
        image by intervals of 90 degrees (rotate_by) and flip it horizontally (flip_Horizontal).
        '''
    #fig = plt.figure()
    hi=np.histogram([i[0] for i in inSpikes],bins=range(0,(size_in*size_in)+1))
    reshapeHist = hi[0].reshape(size_in,size_in)
    rotated = np.rot90(reshapeHist,rotate_by)
    if flip_Horizontal:
        rotated=np.fliplr(rotated)
    plt.imshow(rotated)
    plt.title("")
    rotated = np.rot90(reshapeHist,3)
    plt.show()


def convertSpikeTrainsToSpikeSourceArrays(Inputsp, totalN=16):
    '''
    This function opens a spiking version of an square sized image and
    converts it pyNN compatible spike source array to be loaded into SpiNNaker.

    Inputs:
        Inputsp is a 2D NUMPY array, 1st column holds the neuron IDs (address),
            2nd column holds the spike-times.
        totalN is the total number of neurons in the input layer, ususally
                28 x 28.

    Returns a pyNN list of spike-times where the length of the list is the
    neuron id and the values stored there are its spike-times.
    TODO
    '''
    pynnSpikes = []
    totalN = totalN * totalN
    # For neurons with 0 spikes
    for i in range(totalN):
        pynnSpikes.append([])

    spiketimes  = [i[1] for i in Inputsp]
    addr        = [int(i[0]) for i in Inputsp]

    #addr = [(a) for a in addr]
    for i in range(len(spiketimes)):
        #print addr[i]
        pynnSpikes[addr[i]].append(float(spiketimes[i]))
    return pynnSpikes

def sortMembranebyTimeSpiNNaker(spinnakerMembrane):
    '''
    This simple method converts a list of membrance traces received from spiNNaker to
    a pyNN-style format.
    TODO: Probably I need to add this to the SpiNNaker Package.
    '''
    memArray = (spinnakerMembrane)
    memIndex = np.lexsort((memArray[:,1],memArray[:,0]))
    memArraySorted = memArray[memIndex]
    return memArraySorted


def generateInputStimulusOrientationBars(sizeImg=16,noise=False,min=0.7,max=1.0):
	'''
	creates a square image sizeImg x sizeImg of 4 orientation bars.
	Returns a list of orientation bar images where
	stimulusBar[0] -> horizontal bar
	stimulusBar[1] -> diagonal
	stimulusBar[2] -> vertical
	stimulusBar[3] -> diagonal flip
	'''
	#------------------------------------------------
	# Input stimuli
	#------------------------------------------------
	np.random.seed(seed=7)
	#min = 0.7
	#max = 1.0
	vertical = np.zeros((sizeImg,sizeImg))
	horizontal = np.zeros((sizeImg,sizeImg))
	diagonal = np.zeros((sizeImg,sizeImg))
	diagonal_inv = np.zeros((sizeImg,sizeImg))
	# Randomize the pixels so they are not all 1

	randomVal = np.random.uniform(min, max, sizeImg*sizeImg)
	randomVal = randomVal.reshape((sizeImg,sizeImg))

	vertical[:,sizeImg/2] = 1.0  #vertical line
	vertical = vertical * randomVal
	horizontal[sizeImg/2,:] = 1.0 #horizontal line
	horizontal = horizontal * randomVal
	np.fill_diagonal(diagonal,1)
	diagonal = diagonal * randomVal
	diagonal_flip = np.fliplr(diagonal)
	stimulusBar = []
	stimulusBar.append(horizontal)
	stimulusBar.append(diagonal)
	stimulusBar.append(vertical)
	stimulusBar.append(diagonal_flip)
	return stimulusBar

def generateInputStimulusOrientationBarsThicker(sizeImg=16,noise=False,min=0.9,max=1.0):
    '''
    creates a square image sizeImg x sizeImg of 4 orientation bars.
    Returns a list of orientation bar images where
    stimulusBar[0] -> horizontal bar
    stimulusBar[1] -> diagonal
    stimulusBar[2] -> vertical
    stimulusBar[3] -> diagonal flip
    '''
    np.random.seed(seed=8)
    stimulusBar=[]
    q=np.random.uniform(0.8,1.0,(3,12))
    horbar = np.zeros((16,16))
    verbar = np.zeros((16,16))
    horbar[7:10,2:14]=1.0
    horbar[7:10,2:14] = horbar[7:10,2:14] * q
    verbar=np.rot90(horbar)
    diagBar=sc.interpolation.rotate(horbar,45,reshape=False,mode='constant',order=0,prefilter=False) #45
    diagBar_flip = np.fliplr(diagBar)
    stimulusBar.append(horbar)
    stimulusBar.append(diagBar)
    stimulusBar.append(verbar)
    stimulusBar.append(diagBar_flip)
    return stimulusBar


def intensity_to_delay_convertion(mnist_image,symbol_time):
    maxintensity=max(mnist_image.flatten())
    delays = (maxintensity-mnist_image)*symbol_time
    removethese=np.where(delays>=maxintensity*symbol_time)
    delays[removethese[0],removethese[1]]=-1000
    addr=range(0,len(delays.flatten()))
    spikes = np.vstack([delays.flatten(),addr])
    spikes=spikes.transpose()
    keepthese= np.where(spikes[:,0]!=-1000)
    spikes=spikes[keepthese[0]].astype("int")
    # sort by timestamps
    spikes=np.sort(spikes.view("i8,i8"),order=["f0"],axis=0).view(np.int)
    xs=spikes[:,1]% mnist_image.shape[1]
    ys=spikes[:,1]/ mnist_image.shape[0]
    megasim_spikes=np.vstack([spikes[:,0],[-1]*len(spikes),[-1]*len(spikes),xs,ys,[1]*len(spikes)])
    return megasim_spikes.transpose()


def convertOrientationBarsToSpikesRandomOrder(stimulusBar,n_spikes,t_stop,repeatPattern,encoding="poisson",pattern=0):
    '''
    pattern -> 0 for standard order, 1 for random order, 2 for shuffled order
    '''
    # 0-3 is the input pattern
    #stimOrder = np.random.randint(4,size=repeatPattern)
    #print " STIMULUS ORDER"
    #print stimOrder
    np.random.seed(999)
    if pattern==0:
        stimOrder = [0,1,2,3] * repeatPattern
        stimOrder = np.asarray(stimOrder)
    elif pattern ==1:
        stimOrder = np.random.randint(4,size=repeatPattern)
    elif pattern ==2:
        stimOrder = []
        for rd in range(repeatPattern/4):
            stim=range(4)
            np.random.shuffle(stim)
            stimOrder.append(stim)
        stimOrder = [item for sublist in stimOrder for item in sublist]
    print stimOrder

    stimulusSpikes=[]
    teacherSpikes = []
    for i in range(repeatPattern):
        if encoding=="poisson":
            tmp = convertMNISTtoSpikeTrains(stimulusBar[stimOrder[i]],n_spikes=n_spikes,t_stop=t_stop,asInMatlab=False)
        elif encoding =="delay":
            pass
        else:
            raise Exception("select a spike encoding scheme")
        tmp[:,1]=tmp[:,1]+(i*presentNextStimulusAfter)
        tmpt=np.copy(tmp)
        tmpt[:,0]=stimOrder[i]
        #tmpt[:,4]=0
        stimulusSpikes.append(tmp)
        teacherSpikes.append(tmpt)
    stimulusSpikes = np.concatenate(stimulusSpikes)
    teacherSpikes = np.concatenate(teacherSpikes)

    megastim = np.zeros((len(stimulusSpikes),6),dtype="int" )
    teachermega= np.zeros((len(teacherSpikes),6),dtype="int" )

    # copy time stamps
    megastim[:,0] = stimulusSpikes[:,1]
    megastim[:,1] = np.ones( (len(stimulusSpikes))) *-1
    megastim[:,2] = np.ones( (len(stimulusSpikes))) *-1
    megastim[:,3] = stimulusSpikes[:,0]% inputsize
    megastim[:,4] = stimulusSpikes[:,0]/ inputsize#sensorSize-ys# in megasim Ys are inverted
    megastim[:,5] = np.ones( (len(stimulusSpikes)))

    teachermega[:,0] = teacherSpikes[:,1]
    teachermega[:,1] = np.ones( (len(teacherSpikes))) *-1
    teachermega[:,2] = np.ones( (len(teacherSpikes))) *-1
    teachermega[:,3] = teacherSpikes[:,0]% inputsize
    teachermega[:,4] = teacherSpikes[:,0]/ inputsize#sensorSize-ys# in megasim Ys are inverted
    teachermega[:,5] = np.ones( (len(teacherSpikes)))

    return megastim,teachermega


if withDecay:
    epsilon = 0.02
    wtaInhWeights = -10.0
else:
    epsilon = 0.0006

    wtaInhWeights = -9.0

#------------------------------------------------
# Icdcdcdnput stimuli
#------------------------------------------------
sImgSiz = 16 # 16x16
stimulusBar = generateInputStimulusOrientationBarsThicker(sImgSiz)

# Convert images to spike trains
# stimulusSpikes,stimOrder = convertOrientationBarsToSpikesRandomOrder(
#     stimulusBar, n_spikes, t_stop,repeatInputPattern,2)


#np.savetxt("orientationbars.stim",stimulusSpikes,delimiter=" ",fmt="%d")
#np.savetxt("teacher.stim",stimOrder,delimiter=" ",fmt="%d")


forward_pass = np.load("tuningCurves_inSpikes_step10deg.npy")
stimulusSpikes=[]
for i in range(len(forward_pass)):
    tmp = convertMNISTtoSpikeTrains(np.asarray(
        forward_pass[i,:]/float(max(forward_pass[i,:]))).reshape((16,16)),
    n_spikes=100,t_stop=1,
        asInMatlab=False)
    tmp[:,1]=tmp[:,1]+(i*presentNextStimulusAfter)
    tmpt=np.copy(tmp)
    stimulusSpikes.append(tmp)
stimulusSpikes = np.concatenate(stimulusSpikes)
megastim = np.zeros((len(stimulusSpikes),6),dtype="int" )

# copy time stamps
megastim[:,0] = stimulusSpikes[:,1]
megastim[:,1] = np.ones( (len(stimulusSpikes))) *-1
megastim[:,2] = np.ones( (len(stimulusSpikes))) *-1
megastim[:,3] = stimulusSpikes[:,0]% inputsize
megastim[:,4] = stimulusSpikes[:,0]/ inputsize#sensorSize-ys# in megasim Ys are inverted
megastim[:,5] = np.ones( (len(stimulusSpikes)))


