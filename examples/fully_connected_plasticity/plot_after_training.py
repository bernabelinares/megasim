import numpy as np
import pylab as plt
import scipy.ndimage as sc
import time
import natsort
import matplotlib.cm as cm
import os
import pyNN.spiNNaker as p


inS = np.load("tuningCurves_inSpikes_step10deg.npy")
outS = np.genfromtxt("node_post.evs",delimiter=" ", dtype="int")

# Reverse
#outS = outS[::-1]

outRates = []
for i in range(4):
	outRates.append([x[i] for x in outS])

tuningFig = plt.figure(facecolor='white',figsize=(10,8))
colorbarc = [cm.Purples,cm.Greens,cm.Blues,cm.Reds]
colorcc = ['#9932cc','g','b','r']

pltName=[]
plotR=plt.subplot(111)
for i in range(4):
	pltName.append(plotR.plot(outRates[i],color=colorcc[i],marker='o'))

plt.legend((pltName[0][0],pltName[1][0],pltName[2][0],pltName[3][0]),
	("Neuron 1","Neuron 2","Neuron 3","Neuron 4"),loc=1)
plt.xlabel("Orientation of the bar ($^\circ$)",fontsize=20)
plt.ylabel("Firing Rates (Hz)",fontsize=22)
plt.tick_params(axis='both',which='major',labelsize=13)
plt.xlim((0,17))

plt.xticks(range(18),[str(x) for x in range(0,180,10)])
#plt.tight_layout()
tuningFig.suptitle("Orientation tuning curves", fontsize = 24)

plt.savefig("BCM_OrientationTuningCurves.pdf",dpi=900)

A = inS
B = outS 
nNeurons = 4
simTime = len(outS)*1000
presentNextStimulusAfter = 1000
repeatInputPattern = len(outS)
rateFig = plt.figure(facecolor='white')
import matplotlib.cm as cm

plotR=plt.subplot(212)
scalem = 1000
wid=0.25 * scalem
for p in range(len(outS)):
    for i in range(nNeurons):
        postpop = plt.bar((i*wid)+(p*scalem),B[p][i],wid,color=colorcc[i])
plt.xlabel("")
plt.ylabel("# of Spikes")
plotR.legend(["Neuron 1","Neuron 2","Neuron 3","Neuron 4"],loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)
#box = plotR.get_position()


plotR=plt.subplot(211,sharex=plotR)
plt.setp(plotR.get_yticklabels(),visible=False)
plt.setp(plotR.get_xticklabels(),visible=False)
plotR.set_xlim((0,simTime))
plotR.set_ylim((0,presentNextStimulusAfter))
plt.title("Input Stimuli")

for i in range(repeatInputPattern):
    plotR.imshow(A[i].reshape((16,16)),cmap=cm.Greys,
        extent=[presentNextStimulusAfter*i,presentNextStimulusAfter*(i+1),  0,presentNextStimulusAfter],interpolation='none')
    # plot vertical lines to seperate images
    plotR.plot([presentNextStimulusAfter*i, presentNextStimulusAfter*i],[0,presentNextStimulusAfter],'black')




plt.show()
