.netlist
source {input} i.txt
module_conv {input} {l1_1} gabor_01.prm gab_001.stt
module_conv {input} {l1_2} gabor_02.prm gab_002.stt
module_conv {input} {l1_3} gabor_03.prm gab_003.stt
module_conv {input} {l1_4} gabor_04.prm gab_004.stt

.options
Tmax = 429290101
