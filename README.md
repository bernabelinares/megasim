# MEGASIM #

Modular Event-driven Growing Asynchronous Simulator (MegaSim) is a tool designed for simulating Address-event
representation (AER) systems.  In MegaSim the user may define a topology by utilizing modules that process input
events, nodes to connect modules with each other and input stimuli sources for example data recorded from a DVS
retina, or generated artificially by some algorithm. Modules can have an arbitrary number of input/output ports
and can be either population of neurons or any algorithm described in C.  Modules communicate through nodes by
utilizing the AER events. In MegaSim it is assumed that events are communicated from an output port to an input
port through asynchronous communication using Rqst and Ack signals, where there would be a delay between Rqst and
Ack. A handshake-less link can be emulated by simply defining a zero delay for Ack.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

make -f Makefile_megasim

then source the source.sh:
    source source.sh
or add the /bin to your path

* Dependencies

For the python scripts found in the python_scripts folder:
    Numpy version       >= 1.10.4
    Matplotlib version  >= 1.5.0
    
* How to run tests

Tests are in the test_modules/ folder but I need to create a python script to automate the process

* Examples

Examples are in the examples/ folder

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines


Copyright (C) 2016 CSIC, Instituto de Microelectronica de Sevilla,
Author: Bernabe Linares-Barranco
